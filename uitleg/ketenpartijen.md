# Ketenpartijen

![fig-keten](./assets/ketenpartijen.png)

| Ketenpartij | Omschrijving | Onderdeel proces |
|-|-|-|
| CAK | Stelt de eigen bijdrage voor zorg en ondersteuning vast die de client moet betalen, en brengt deze in rekening. | Financiering |
| CIZ | Vervult de functie van indicatieorgaan. Beoordeelt op basis van de zorgbehoefte van de client of deze in aanmerking komt voor de zorg en controleert tevens bij de SVB of de client recht heeft op zorg. |
