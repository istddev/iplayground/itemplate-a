# Informatiemodel TEMPLATE A

Dit is het Informatiemodel voor de *TEMPLATE* van ... Nederland.
U vindt hier de specificaties van de *iTEMPLATE* standaard release x.y.z  
  
Het Informatiemodel brengt de samenhang in processen, bedrijfsregels en berichtspecificaties in kaart.
Deze elementen zijn onlosmakelijk met elkaar verbonden: ze versterken óf beperken elkaar. Het Informatiemodel *TEMPLATE* geeft een integraal overzicht van het *TEMPLATE*-berichtenverkeer zoals het er vanaf *d mmmmm jjjj* uitziet.  
  
*iTEMPLATE* is onderdeel van de familie van *iTemplates* die beheerd wordt door ... Nederland. Door een integrale
aanpak van de doorontwikkeling van de *iTemplates* bewaakt het ... de onderlinge consistentie van de
standaarden.
