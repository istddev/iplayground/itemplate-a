# Summary iTemplate A

## Uitleg

* [Informatiemodel](README.md)
* [Metamodel](uitleg/metamodel.md)
* [Ketenpartijen](uitleg/ketenpartijen.md)

## Processen

* [Ketenproces](processen/ketenproces.md)